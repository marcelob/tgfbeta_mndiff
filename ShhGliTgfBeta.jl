# written by Marcelo Boareto (03/2019) marcelo.boareto@bsse.ethz.ch

using DifferentialEquations, Plots, Statistics, Random, Distributions
include("functions.jl")

function eqs(du,u,p,t)
    u1=u[1,:,:]
    u2=u[2,:,:]
    u3=u[3,:,:]
    # ------------- for the bifurcation analysis -------------------------------
    if p[1]==1.0
        u2=p[2]
    end
    # ------------------------------------------------------------------------------------
    # ---------------------------- spatial averaging ----------------------------------------
    if p[3]==1.0  # nearest neighboring averaging
        a = zeros(N+2,N+2)
        a[2:(end-1),2:(end-1)] = u[3,:,:]
        # ------------------------  periodic boundary conditions -----------------------
        a[1,2:(end-1)]   = u[3,end,:]
        a[2:(end-1),1]   = u[3,:,end]
        a[end,2:(end-1)] = u[3,1,:]
        a[2:(end-1),end] = u[3,:,1]
        # --------------------------------------------------------------------------------
        u3=0.5*(u[3,:,:] .+ 0.25*(a[2:(end-1),1:(end-2)].+a[2:(end-1),3:end].+a[1:(end-2),2:(end-1)].+a[3:end,2:(end-1)]) )
    elseif p[3]==2.0  # population averaging
        u3 = mean(u[3,:,:])
    end
    # ------------------------------------------------------------------------------------
    # ------------ imposes a correlation between GliA and GliR ---------------------------
    if p[4]>0.0
        u1=p[4]*u[2,:,:]
    end
    # ------------------------------------------------------------------------------------

    # Equations for GliA, GliR, TgfB, and Phox2b respectively
    du[1,:,:] = v[1]*u[1,:,:]
    du[2,:,:] = v[2]*u[2,:,:]
    du[3,:,:] = v[3]*((1.0-e[3])*HSp.(u1,h[1],n[1],e[1]).*HSn.(u2,h[2],n[2],e[2]) .+ e[3]*Hp.(u3,h[3],n[3]) .- u[3,:,:])
    du[4,:,:] = v[4]*(HSp.(u1,h[4],n[4],e[4]).*HSn.(u3,h[5],n[5],e[5]) .- u[4,:,:])
end

function standard_parameters(noise_ic;c="",p3=0.0)
    v  = [-log(2)/0.8,-log(2)/0.8,50.0,90.0]  # "velocity" of the equation
    u0 = [1.0,1.0,0.0,1.0].*noise_ic          # initial conditions
    n  = [2.0,2.0,2.0,2.0,2.0]                # Hill coefficient
    p  = [0.0,0.0,p3,0.0]                     # bifurcation (p[1] and p[2]), spatial averaging (p[3]), and correlation GliA,R (p[4])
    if c in ["GliA->Phox2b","GliR-|TgfB-|Phox2b_noSAnoHyst","GliR-|TgfB-|Phox2b_SAnoHyst","GliR-|TgfB-|Phox2b_SAHyst",""]
        e = [0.0,1.0,0.4,0.0,1.0]             # whether the Hill function is being considered (see HSn,HSp definition)
        h = [0.2,0.2,0.3,0.2,0.3]             # Hill factors (threshould)
        if c=="GliA->Phox2b"
            e=[0.0,0.0,0.0,1.0,0.0]
            h[4]=0.25
        elseif c=="GliR-|TgfB-|Phox2b_noSAnoHyst"
            e[3]=0.0
            h[2]=0.15
        elseif c=="GliR-|TgfB-|Phox2b_SAnoHyst"
            h[2]=0.1
        elseif c=="GliR-|TgfB-|Phox2b_SAHyst"
            e[3]=0.94
        end

    elseif c in ["WT","WT_uncorr","TgfB_OFF","Gli1_OFF"]
        e = [1.0,1.0,0.94,1.0,1.0]
        h = [0.08,0.2,0.3,0.08,0.3]
        p[4] = 1.0
        if c=="WT_uncorr"
            p[4]=0.0
        elseif c=="TgfB_OFF"
            e=[0.0,0.0,0.0,1.0,0.0]
            v[3]=0.0
        elseif c=="Gli1_OFF"
            p[4]=0.7
        end
    end
    return v,u0,h,n,e,p
end

#-----------------------------------------------------------------------------------------------------------------------
N = 100           # (N,N) cells
neqs = 4          # number of equations
tr = (3.5,7.5)    # time range
dt = 0.01         # saving solution of each dt step
t  = tr[1]:dt:tr[2]
r_nlevel = 0.0:0.05:0.5
ni = 7
println("standard kinetic noise level=$(r_nlevel[ni])")
noise_ic  = ones(neqs,N,N).+r_nlevel[ni]*randn(neqs,N,N)
global v,u0,h,n,e,p=standard_parameters(noise_ic)

r_v = -log(2)./(0.3:0.2:1.5)
r_c = ["GliA->Phox2b","GliR-|TgfB-|Phox2b_SAnoHyst","GliR-|TgfB-|Phox2b_noSAnoHyst","GliR-|TgfB-|Phox2b_SAHyst","WT","WT_uncorr"]
Ml = zeros(length(r_c),length(r_v),neqs,5,length(t))
@time for j in 1:length(r_c)
    for i in 1:length(r_v)
        global nlevel=r_nlevel[ni]

        global v,u0,h,n,e,p=standard_parameters(noise_ic;c=r_c[j],p3=1.0)
        v[1] = r_v[i]
        v[2] = r_v[i]
        Ml[j,i,:,:,:]=solve_equations(eqs,noise,v,h,n,e,u0,p)
    end
end

r_tr = 10.0.^(-2.3:0.1:-0.3)
r_c = ["GliA->Phox2b","GliR-|TgfB-|Phox2b_noSAnoHyst","GliR-|TgfB-|Phox2b_SAnoHyst","GliR-|TgfB-|Phox2b_SAHyst"]
Mtr=zeros(length(r_c),length(r_tr),neqs,5,length(t))
@time for i in 1:length(r_tr)
    global nlevel=r_nlevel[1]
    noise_ic  = ones(neqs,N,N).+nlevel*randn(neqs,N,N)

    for j in 1:length(r_c)
        global v,u0,h,n,e,p=standard_parameters(noise_ic;c=r_c[j])
        Mtr[j,i,:,:,:] = solve_equations(eqs,noise,v,h,n,e,u0,p;n_tr=r_tr[i])
    end
end


r_c = ["GliA->Phox2b","GliR-|TgfB-|Phox2b_noSAnoHyst","GliR-|TgfB-|Phox2b_SAnoHyst","GliR-|TgfB-|Phox2b_SAHyst",
       "WT","WT_uncorr","Gli1_OFF","TgfB_OFF"]
Mp0 = zeros(length(r_c),length(r_nlevel),neqs,5,length(t))
@time for i in 1:length(r_nlevel)
    global nlevel=r_nlevel[i]
    noise_ic  = ones(neqs,N,N).+nlevel*randn(neqs,N,N)

    for j in 1:length(r_c)
        global v,u0,h,n,e,p=standard_parameters(noise_ic;c=r_c[j],p3=0.0)
        Mp0[j,i,:,:,:] = solve_equations(eqs,noise,v,h,n,e,u0,p)
    end
end
Mp1 = zeros(length(r_c),length(r_nlevel),neqs,5,length(t))
@time for i in 1:length(r_nlevel)
    global nlevel=r_nlevel[i]
    noise_ic  = ones(neqs,N,N).+nlevel*randn(neqs,N,N)

    for j in 1:length(r_c)
        global v,u0,h,n,e,p=standard_parameters(noise_ic;c=r_c[j],p3=1.0)
        Mp1[j,i,:,:,:] = solve_equations(eqs,noise,v,h,n,e,u0,p)
    end
end
println("Simulations done.")
println("")

#-----------------------------------------------------------------------------------------------------------------------

pA=plot_expression_withCI(t,Mp0[1,ni,1,:,:];ylabel="GliA,GliR",color="#fb4f4fff",show_thr=false)
pB=plot_expression_withCI(t,Mp0[1,ni,4,:,:];ylabel="Phox2b",color="#fb4f4fff",show_thr=true,tr=0.4)
pB=plot_expression_withCI(t,Mp0[2,ni,4,:,:];ylabel="Phox2b",color="#99d8ddff",plt=pB,show_thr=false)
pB=plot_expression_withCI(t,Mp0[3,ni,4,:,:];ylabel="Phox2b",color="#02587bff",plt=pB,show_thr=false)
pB=plot_expression_withCI(t,Mp0[4,ni,4,:,:];ylabel="Phox2b",color="#373e48ff",plt=pB,show_thr=false)
pC=plot_fractionCells([t t t t],[Mp0[1,ni,4,5,:] Mp0[2,ni,4,5,:] Mp0[3,ni,4,5,:] Mp0[4,ni,4,5,:]];
                       color=["#fb4f4fff" "#99d8ddff" "#02587bff" "#373e48ff"],line=:solid)
hline!([0.05,0.95],lw=3.5,color="#373e48ff",line=:dot)
hline!([0.5],lw=3.5,color="#373e48ff",line=:dash)

pD=plot_expression_withCI(t,Mp0[2,ni,3,:,:];ylabel="TgfB",color="#99d8ddff",show_thr=false)
pD=plot_expression_withCI(t,Mp0[3,ni,3,:,:];ylabel="TgfB",color="#02587bff",plt=pD,show_thr=false)
pD=plot_expression_withCI(t,Mp0[4,ni,3,:,:];ylabel="TgfB",color="#373e48ff",plt=pD,show_thr=false)

v,u0,h,n,e,p=standard_parameters(ones(1),c="")
pE=bifurcation_analysis(v,u0,h,n,e,p)
xticks!(pE,[0.0,0.5,1.0])

pF= plot(1.0e-2./r_tr,[DeltaDiff(t,Mtr[1,i,4,5,:]) for i in 1:length(r_tr)],lw=3.5,color="#fb4f4fff",line=:solid)
plot!(pF,1.0e-2./r_tr,[DeltaDiff(t,Mtr[2,i,4,5,:]) for i in 1:length(r_tr)],lw=3.5,color="#99d8ddff",line=:solid)
plot!(pF,1.0e-2./r_tr,[DeltaDiff(t,Mtr[3,i,4,5,:]) for i in 1:length(r_tr)],lw=3.5,color="#02587bff",line=:solid)
plot!(pF,1.0e-2./r_tr,[DeltaDiff(t,Mtr[4,i,4,5,:]) for i in 1:length(r_tr)],lw=3.5,color="#373e48ff",line=:solid)
yticks!(pF,[0.0,1.0,2.0])
xticks!(pF,[0.0,1.0,2.0])
ylabel!("switching period (days)")
xlabel!("threshould noise level")

ylimtr=(-0.1,4.2)
pG= plot(r_nlevel,[DeltaDiff(t,Mp0[1,i,4,5,:]) for i in 1:length(r_nlevel)],lw=3.5,color="#fb4f4fff",line=:solid,ylim=ylimtr)
plot!(pG,r_nlevel,[DeltaDiff(t,Mp0[2,i,4,5,:]) for i in 1:length(r_nlevel)],lw=3.5,color="#99d8ddff",line=:solid)
plot!(pG,r_nlevel,[DeltaDiff(t,Mp0[3,i,4,5,:]) for i in 1:length(r_nlevel)],lw=3.5,color="#02587bff",line=:solid)
plot!(pG,r_nlevel,[DeltaDiff(t,Mp0[4,i,4,5,:]) for i in 1:length(r_nlevel)],lw=3.5,color="#373e48ff",line=:solid)
vline!([r_nlevel[ni]],lw=3.5,color="#373e48ff",line=:dot)
ylabel!("switching period (days)")
xlabel!("kinetic noise level")

pH= plot(r_nlevel,[DeltaDiff(t,Mp1[4,i,4,5,:]) for i in 1:length(r_nlevel)],lw=3.5,color="#373e48ff",line=:solid)
plot!(pH,r_nlevel,[DeltaDiff(t,Mp1[3,i,4,5,:]) for i in 1:length(r_nlevel)],lw=3.5,color="#02587bff",line=:solid)
plot!(pH,r_nlevel,[DeltaDiff(t,Mp1[2,i,4,5,:]) for i in 1:length(r_nlevel)],lw=3.5,color="#99d8ddff",line=:solid,ylim=ylimtr)
vline!([r_nlevel[ni]],lw=3.5,color="#373e48ff",line=:dot)
ylabel!("switching period (days)")
xlabel!("kinetic noise level")

pI=plot_expression_withCI(t,Mp1[6,ni,4,:,:];ylabel="Phox2b",color="#f4ba00ff",show_thr=true,tr=0.4)
pI=plot_expression_withCI(t,Mp1[4,ni,4,:,:];ylabel="Phox2b",color="#373e48ff",plt=pI,show_thr=false)
pI=plot_expression_withCI(t,Mp1[5,ni,4,:,:];ylabel="Phox2b",color="#c7d86eff",plt=pI,show_thr=false)

ylimtr=(-0.1,4.2)
pJ= plot(r_nlevel,[DeltaDiff(t,Mp1[6,i,4,5,:]) for i in 1:length(r_nlevel)],lw=3.5,color="#f4ba00ff",line=:solid,ylim=ylimtr)
plot!(pJ,r_nlevel,[DeltaDiff(t,Mp1[4,i,4,5,:]) for i in 1:length(r_nlevel)],lw=3.5,color="#373e48ff",line=:solid)
plot!(pJ,r_nlevel,[DeltaDiff(t,Mp1[5,i,4,5,:]) for i in 1:length(r_nlevel)],lw=3.5,color="#c7d86eff",line=:solid)
vline!([r_nlevel[ni]],lw=3.5,color="#373e48ff",line=:dot)
ylabel!("switching period (days)")
xlabel!("kinetic noise level")

pK=plot_fractionCells([t t t],[Mp1[8,ni,4,5,:] Mp1[4,ni,4,5,:] Mp1[6,ni,4,5,:] Mp1[7,ni,4,5,:] Mp1[5,ni,4,5,:]];
                       color=["#fb4f4fff" "#373e48ff" "#f4ba00ff" "#373e48ff" "#c7d86eff"],line=:solid)
hline!([1.0],lw=3.5,color="#6f8a91ff",line=:solid)
hline!([0.5],lw=3.5,color="#373e48ff",line=:dash)
hline!([0.05,0.95],lw=3.5,color="#373e48ff",line=:dot)

pL=plot(-1.0./r_v,[t[Ml[1,i,4,2,:].>0.5][end] for i in 1:length(r_v)],lw=3.5,color="#fb4f4fff",line=:solid,
        ylabel="time of switch (days)",xlabel="1/decay rate (days)")
plot!(pL,-1.0./r_v,[t[Ml[5,i,4,2,:].>0.5][end] for i in 1:length(r_v)],lw=3.5,color="#c7d86eff",line=:solid)
plot!(pL,-1.0./r_v,[t[Ml[4,i,4,2,:].>0.5][end] for i in 1:length(r_v)],lw=3.5,color="#373e48ff",line=:solid)
vline!([-1.0/v[1]],lw=3.5,color="#373e48ff",line=:dash)

plot(pA,pB,pC,pE,pD,pF,pG,pH,pJ,pI,pK,pL,layout=grid(4,3),size=(3*300,4*220),legend=false,grid=false,
     guidefont=12,tickfont=12,fmt=:pdf)
# savefig("./figures/FigureMainF.pdf")
